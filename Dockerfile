FROM registry.access.redhat.com/ubi8:php74

# Add application sources to a directory that the assemble script expects them
# and set permissions so that the container runs without root access.
USER 0
ADD . /tmp/src
RUN chown -R 1001:0 /tmp/src
USER 1001

# Configure question2answer.
RUN /tmp/src/configure.sh

# Install the dependencies and assemble.
RUN /usr/libexec/s2i/assemble

# Set the default command for the resulting image.
CMD /usr/libexec/s2i/run