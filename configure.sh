#!/bin/bash -x
export APP_DATA=${APP_DATA:="$(pwd)"}

export HYBRID_AUTH_VERSION=${HYBRID_AUTH_VERSION:-'v2.14.0'}
export OPEN_LOGIN_VERSION=${OPEN_LOGIN_VERSION:-'v3.0.0'}
export Q2A_VERSION=${Q2A_VERSION:-'v1.8.3'}

q2a(){
    trap exit ERR
    echo "-----| q2a"
#    local ARCHIVE="archives/question2answer-${Q2A_VERSION}.tar.gz"

#    if [ -f "${ARCHIVE}" ];
#    then
#        tar -xzf "${ARCHIVE}" --strip-components 1
#    else
#        curl  --silent --location https://github.com/q2a/question2answer/tarball/"${Q2A_VERSION}" | \
#        tar -xzf - --strip-components 1
#    fi
#
#    touch .s2i-"${Q2A_VERSION}"

    cat > qa-config.php << EOF
<?php

define('QA_MYSQL_HOSTNAME', getenv('MYSQL_HOST'));
define('QA_MYSQL_USERNAME', getenv('MYSQL_USER'));
define('QA_MYSQL_PASSWORD', getenv('MYSQL_PASSWORD'));
define('QA_MYSQL_DATABASE', getenv('MYSQL_DATABASE'));

define('QA_EXTERNAL_USERS', false);

define('QA_MYSQL_TABLE_PREFIX', 'qa_');

define('QA_HTML_COMPRESSION', false);
define('QA_MAX_LIMIT_START', 19999);
define('QA_IGNORED_WORDS_FREQ', 10000);
define('QA_ALLOW_UNINDEXED_QUERIES', false);
define('QA_OPTIMIZE_DISTANT_DB', false);
define('QA_PERSISTENT_CONN_DB', false);
define('QA_DEBUG_PERFORMANCE', false);

?>
EOF
    cat qa-config.php
}

open_login(){
    trap exit ERR
    echo "-----| open-login"
    local ARCHIVE="archives/q2a-open-login-${OPEN_LOGIN_VERSION}.tar.gz"

    if [ -d qa-plugin ];
    then
        mkdir -p "${APP_DATA}/open-login-${OPEN_LOGIN_VERSION}"

    if [ -f "${ARCHIVE}" ];
    then
        tar -xzf "${ARCHIVE}" \
            -C open-login-"${OPEN_LOGIN_VERSION}" \
        --strip-components 1
    else
        curl  --silent --location \
            https://github.com/alixandru/q2a-open-login/archive/master.zip | \
        tar -xzf - -C "open-login-${OPEN_LOGIN_VERSION}" --strip-components 1
    fi


        rm -rf qa-plugin/open-login
        ln -s "${APP_DATA}/open-login-${OPEN_LOGIN_VERSION}" qa-plugin/open-login
        touch "${APP_DATA}/qa-plugin/open-login/.s2i-${OPEN_LOGIN_VERSION}"
        ls -l qa-plugin/
    fi

    touch qa-plugin/open-login/providers.php
    chmod 666 qa-plugin/open-login/providers.php
    echo -n '<?php return "Google" ?>' > qa-plugin/open-login/providers.php
}

hybrid(){
    trap exit ERR
    echo "-----| hybrid"
    local ARCHIVE="archives/hybridauth-${HYBRID_AUTH_VERSION}.tar.gz"

    if [ -d qa-plugin/open-login ];
    then
        mkdir -p "${APP_DATA}"/Hybrid-"${HYBRID_AUTH_VERSION}"
    if [ -f "${ARCHIVE}" ];
    then
        tar -xzf "${ARCHIVE}" \
            -C Hybrid-"${HYBRID_AUTH_VERSION}" \
        --strip-components 1
    else
        curl  --silent --location \
            https://github.com/hybridauth/hybridauth/tarball/"${Q2A_VERSION}" | \
        tar -xzf - -C Hybrid-"${HYBRID_AUTH_VERSION}" --strip-components 1
    fi

        rm -rf qa-plugin/open-login/Hybrid
        ln -s "${APP_DATA}"/Hybrid-"${HYBRID_AUTH_VERSION}"/hybridauth/Hybrid qa-plugin/open-login/
        touch "${APP_DATA}"/qa-plugin/open-login/Hybrid/.s2i-"${HYBRID_AUTH_VERSION}"
        ls -l qa-plugin/open-login/
    fi
}

copy_httpd_custom_configs(){
    trap exit ERR

    if [ -d "${APP_DATA}"/httpd-cfg ];
    then
        if [ "$(ls -A "${APP_DATA}"/httpd-cfg/*.conf)" ];
        then
            echo "---> Copying custom config files from httpd-cfg"
            cp -v "${APP_DATA}"/httpd-cfg/*.conf "${HTTPD_CONFIGURATION_PATH}"/
        else
            echo "---> No files *.conf found in ${APP_DATA}/httpd-cfg/"
        fi
    fi
}

# ----------------
#
#echo "Before assembling"
#
#/usr/libexec/s2i/assemble
#rc=$?
#
#if [ $rc -eq 0 ]; then
    #echo "After successful assembling"

    q2a
    #open_login #Broken, deal with it later
    #hybrid
    #copy_httpd_custom_configs

#fi

#exit $rc
